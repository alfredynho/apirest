# Api Proyecto APi rest
Alfredo Callizaya Gutierrez

| Methods	| Urls	|
| -------- | ------- 
| GET | api/producto 
| GET | api/producto/:id
| POST | api/producto 
| PUT | api/producto/:id 
| DELETE | api/producto/:id 

## instalar paquetes
```
npm install
```

### Correr Proyecto
```
npm run run dev
```
