import app from './setup.js'
import { sequelize } from './database/database.js';
import 'dotenv/config'

async function main(){
    console.clear();
    await sequelize.sync({ force:false})    

const PORT = process.env.PORT;
app.listen(PORT);

console.log('Server listening en' ,PORT);

}

main();