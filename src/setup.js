
import express from 'express';
import morgan from 'morgan';

const app=express()
 
import productoRoutes from './routes/producto.routes.js'
import usuarioRoutes from './routes/usuario.routes.js'

app.use(morgan('dev'));
app.use(express.json());

app.use('/api/usuario',usuarioRoutes);
app.use('/api/producto',productoRoutes);

export default app;